const controller = require("../controllers/book.controller");
const { authJwt, filter, admin } = require("../middleware");
const { body, query } = require('express-validator')
const multer = require('multer')
var upload = multer({ dest: `${__dirname}/../export/static/library/` })

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/book/all", [
        authJwt.verifyToken,
        filter.dormCheck
    ], controller.list);

    app.get("/api/book/get", [
        authJwt.verifyToken,
        query('bookId').isLength({ min: 1, max: 100 })
    ], controller.get);

    app.post("/api/book/create", [
        authJwt.verifyToken,
        admin.isLibraryAdmin,
        body('id').isLength({ min: 1, max: 100 }),
        body('title').isLength({ min: 1 }),
        body('author').isLength({ min: 1 }),
        body('description').isLength({ min: 1 }),
        body('pages').isNumeric(),
        body('buildingId').isNumeric(),
    ], controller.create);

    app.post("/api/book/setpicture/:id", upload.single("file"), [
        authJwt.verifyToken,
        admin.isLibraryAdmin,
    ], controller.setpicture);

    app.post("/api/book/edit", [
        authJwt.verifyToken,
        admin.isLibraryAdmin,
        body('bookId').isLength({ min: 1, max: 100 }),
        body('title').isLength({ min: 1 }),
        body('author').isLength({ min: 1 }),
        body('description').isLength({ min: 1 }),
        body('pages').isNumeric(),
        body('buildingId').isNumeric(),
    ], controller.edit);

    app.post("/api/book/destroy", [
        authJwt.verifyToken,
        admin.isLibraryAdmin,
        body('bookId').isNumeric()
    ], controller.destroy);
}
