module.exports = (sequelize, Sequelize) => {
    const Item = sequelize.define("items", {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        stock: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        price: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        iconId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        gender: {
            type: Sequelize.ENUM(['Male', 'Female']),
            allowNull: false,
            defaultValue: 'Male'
        }
    });

    return Item;
};
