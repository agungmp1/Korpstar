module.exports = (sequelize, Sequelize) => {
    const AchievementRecord = sequelize.define("achievement_records", {
        name: {
            type: Sequelize.STRING(126).BINARY,
        },
        iconId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        type: {
            type: Sequelize.STRING
        }
    });

    return AchievementRecord;
};
