module.exports = (sequelize, Sequelize) => {
    const Icon = sequelize.define("icons", {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        filename: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, { timestamps: false });

    return Icon;
};
