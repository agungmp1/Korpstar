module.exports = (sequelize, Sequelize) => {
    const CTFChallenge = sequelize.define("ctf_challenges", {
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false
        },
        point: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        flag: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return CTFChallenge;
};
