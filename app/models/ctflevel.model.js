module.exports = (sequelize, Sequelize) => {
    const CTFLevel = sequelize.define("ctf_levels", {
        name: {
            type: Sequelize.STRING,
            unique: true
        }
    }, { timestamps: false });

    return CTFLevel;
};
