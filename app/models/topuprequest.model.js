module.exports = (sequelize, Sequelize) => {
    const TopUpRequest = sequelize.define("topup_requests", {
        midtranstransactionid:{
            type: Sequelize.STRING,
            allowNull: false
        },
        midtranspaymenttype:{
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return TopUpRequest;
};
