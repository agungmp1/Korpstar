const db = require("../models");
const Aspiration = db.aspiration;
const Vote = db.vote;
const User = db.user;
const Class = db.class;
const Role = db.role;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');
const fs = require('fs-extra');

exports.recap = (req, res) => {
    Aspiration.findAll({
        where: {[Op.and]: [{isActive: true}, {isAccepted: true}]},
        attributes: ['id'],
        include: [{
            model: Vote,
            attributes: ['userNpm']
        }]
    }).then( aspirations => {
        let voted = 0;
        aspirations.forEach((aspiration) => {
            aspiration.votes.forEach((vote) => {
                if (vote.userNpm == req.npm) {
                    voted++
                }
            });
        });
        res.status(200).send({voted: voted, total: aspirations.length});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.all = (req, res) => {
    Aspiration.findAll({
        where: {isAccepted: true},
        attributes: ['id', 'title', 'argument', 'suggestion', 'proof', 'target', 'isAnonymous', 'isActive', 'createdAt'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname']
        },{
            model: Vote,
            attributes: ['userNpm','value']
        }]
    }).then( aspirations => {
        let aspirationData = [];
        aspirations.forEach((aspiration) => {
            let myvote = 0;
            let upvote = 0;
            let downvote = 0;
            aspiration.votes.forEach((vote) => {
                if (vote.userNpm == req.npm) {
                    myvote = vote.value;
                }
                if(vote.value==1){upvote++}else{downvote++};
            });
            aspirationData.push({
                id: aspiration.id,
                title: aspiration.title,
                argument: aspiration.argument,
                suggestion: aspiration.suggestion,
                proof: aspiration.proof,
                target: aspiration.target,
                isAnonymous: aspiration.isAnonymous,
                isActive: aspiration.isActive,
                upvote: upvote,
                downvote: downvote,
                createdAt: aspiration.createdAt,
                user: aspiration.isAnonymous? {npm: '', fullname: 'Anonymous'}:aspiration.user,
                myvote: myvote
            });
        });
        User.findOne({
            where: {npm: req.npm},
            include: {
                model: Role,
                attributes: ['id']
            }
        }).then(user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 9) {
                    isAdmin = true;
                }
            });
            res.status(200).send({data: aspirationData, isAdmin: isAdmin});
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const aspirationio = require(`${__dirname}/../socket/aspiration.socket`);

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.create({
        title: req.body.title,
        argument: req.body.argument,
        suggestion: req.body.suggestion,
        proof: req.body.proof,
        target: req.body.target,
        isAnonymous: req.body.isAnonymous,
        userNpm: req.npm,
        isAccepted: false,
        isActive: false
    }).then(aspiration => {
        User.findOne({
            where: {npm: req.npm}, attributes: ['fullname']
        }).then(user => {
            let date = new Date(aspiration.createdAt);
            console.log(`[new aspiration][${new Date()}] ${req.npm} create new aspiration #${aspiration.id}`);
            res.status(200).send({ message: "Created sucessfully!" });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });

};

exports.destroy = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findOne({ where: {id: req.body.aspirationId}}).then( data => {
        if (req.npm !==data.userNpm) {
            return res.status(403).send({ message: "Can only delete your own aspiration"})
        }
        Aspiration.destroy({ where: {id: req.body.aspirationId}}).then( data => {
            aspirationio.emitdata('dataRemove', {
                data: {
                    id: req.body.aspirationId
                }
            });
            console.log(`[aspiration unsend][${new Date()}] ${req.npm} unsend aspiration #${aspiration.id}`);
            res.status(200).send({ message: "Destroyed successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.clearinactive = (req, res) => {
    Aspiration.findAll({ where: {[Op.and]: [{isActive: false}, {isAccepted: true}]}}).then( aspirations => {
        let toDelete = []
        aspirations.forEach(aspiration => {
            toDelete.push(aspiration.id)
        })
        Vote.destroy({ where: {aspirationId: toDelete}}).then( x => {
            Aspiration.destroy({ where: {id: toDelete}}).then( x => {
                console.log(`[aspiration clear][${new Date()}] ${req.npm} clear aspiration data`);
                res.status(200).send({ message: "Cleared successfully!" });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter;
const zip = require('express-zip');

exports.downloadreport = (req, res) => {
    Aspiration.findAll({
        where: {[Op.and]: [{isActive: false}, {isAccepted: true}]},
        attributes: ['id', 'title', 'argument', 'suggestion', 'proof', 'target', 'isAnonymous', 'isActive', 'createdAt'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname']
        },{
            model: Vote,
            attributes: ['userNpm','value']
        }]
    }).then( aspirations => {
        let aspirationData = [];
        aspirations.forEach((aspiration) => {
            let upvote = 0;
            let downvote = 0;
            aspiration.votes.forEach((vote) => {
                if(vote.value==1){upvote++}else{downvote++};
            });
            aspirationData.push({
                id: aspiration.id,
                title: aspiration.title,
                argument: aspiration.argument,
                suggestion: aspiration.suggestion,
                proof: aspiration.proof,
                target: aspiration.target,
                isAnonymous: aspiration.isAnonymous,
                upvote: upvote,
                downvote: downvote,
                createdAt: aspiration.createdAt,
                user_fullname: aspiration.isAnonymous? 'Anonymous':aspiration.user.fullname,
                user_npm: aspiration.isAnonymous?'':aspiration.user.npm,
            });
        });
        const now = Date.now();
        const csvWriter = createCsvWriter({
            path: `${__dirname}/../export/aspiration/dataaspirasi_${now}.csv`,
            header: [
                { id: "id", title: "id" },
                { id: "title", title: "title" },
                { id: "argument", title: "argument" },
                { id: "suggestion", title: "suggestion" },
                { id: "proof", title: "proof" },
                { id: "target", title: "target" },
                { id: "user_npm", title: "user_npm" },
                { id: "user_fullname", title: "user_fullname" },
                { id: "isAnonymous", title: "isAnonymous" },
                { id: "upvote", title: "upvote" },
                { id: "downvote", title: "downvote" },
                { id: "createdAt", title: "createdAt" },
            ]
        });
        csvWriter.writeRecords(aspirationData).then(() => {
            const file = `${__dirname}/../export/aspiration/dataaspirasi_${now}.csv`;
            console.log(`[aspiration download][${new Date()}] ${req.npm} download aspiration data`);
            res.zip([{
                path: file,
                name: `dataaspirasi_${now}.csv`
            }]);
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.upvote = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findOne({
        where: {id: req.body.aspirationId},
        attributes: ['isActive','isAccepted'],
    }).then( aspiration => {
        if ( !aspiration.isActive || !aspiration.isAccepted) {
            return res.status(422).send({ message: "Not active or not accepted" })
        }
        Vote.destroy({ where: {[Op.and]: [{aspirationId: req.body.aspirationId}, {userNpm: req.npm}]}}).then(x => {
            Vote.create({
                userNpm: req.npm,
                aspirationId: req.body.aspirationId,
                value: 1
            }).then(y => {
                Aspiration.findOne({
                    where: {id: req.body.aspirationId},
                    attributes: ['id'],
                    include: {
                        model: Vote,
                        attributes: ['userNpm','value']
                    }
                }).then( aspiration => {
                    let myvote = 0;
                    let upvote = 0;
                    let downvote = 0;
                    aspiration.votes.forEach((vote) => {
                        if (vote.userNpm == req.npm) {
                            myvote = vote.value;
                        }
                        if(vote.value==1){upvote++}else{downvote++};
                    });
                    aspirationio.emitdata('dataChange', {
                        data: {
                            id: req.body.aspirationId,
                            upvote: upvote,
                            downvote: downvote
                        }
                    });
                    res.status(200).send({ message: "Upvoted successfully!" });
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.downvote = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findOne({
        where: {id: req.body.aspirationId},
        attributes: ['isActive','isAccepted'],
    }).then( aspiration => {
        if ( !aspiration.isActive || !aspiration.isAccepted) {
            return res.status(422).send({ message: "Not active or not accepted" })
        }
        Vote.destroy({ where: {[Op.and]: [{aspirationId: req.body.aspirationId}, {userNpm: req.npm}]}}).then(x => {
            Vote.create({
                userNpm: req.npm,
                aspirationId: req.body.aspirationId,
                value: -1
            }).then(y => {
                Aspiration.findOne({
                    where: {id: req.body.aspirationId},
                    attributes: ['id'],
                    include: {
                        model: Vote,
                        attributes: ['userNpm','value']
                    }
                }).then( aspiration => {
                    let myvote = 0;
                    let upvote = 0;
                    let downvote = 0;
                    aspiration.votes.forEach((vote) => {
                        if (vote.userNpm == req.npm) {
                            myvote = vote.value;
                        }
                        if(vote.value==1){upvote++}else{downvote++};
                    });
                    aspirationio.emitdata('dataChange', {
                        data: {
                            id: req.body.aspirationId,
                            upvote: upvote,
                            downvote: downvote
                        }
                    });
                    res.status(200).send({ message: "Downvoted successfully!" });
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });

}

exports.unvote = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findOne({
        where: {id: req.body.aspirationId},
        attributes: ['isActive','isAccepted'],
    }).then( aspiration => {
        if ( !aspiration.isActive || !aspiration.isAccepted) {
            return res.status(422).send({ message: "Not active or not accepted" })
        }
        Vote.destroy({ where: {[Op.and]: [{aspirationId: req.body.aspirationId}, {userNpm: req.npm}]}}).then(x => {
            Aspiration.findOne({
                where: {id: req.body.aspirationId},
                attributes: ['id'],
                include: {
                    model: Vote,
                    attributes: ['userNpm','value']
                }
            }).then( aspiration => {
                let myvote = 0;
                let upvote = 0;
                let downvote = 0;
                aspiration.votes.forEach((vote) => {
                    if (vote.userNpm == req.npm) {
                        myvote = vote.value;
                    }
                    if(vote.value==1){upvote++}else{downvote++};
                });
                aspirationio.emitdata('dataChange', {
                    data: {
                        id: req.body.aspirationId,
                        upvote: upvote,
                        downvote: downvote
                    }
                });
                res.status(200).send({ message: "Unvoted successfully!" });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.request = (req, res) => {
    Aspiration.findAll({
        where: {isAccepted: false},
        attributes: ['id', 'title', 'argument', 'suggestion', 'proof', 'target', 'isAnonymous', 'isActive', 'createdAt'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname']
        },{
            model: Vote,
            attributes: ['userNpm','value']
        }]
    }).then( aspirations => {
        let aspirationData = [];
        aspirations.forEach((aspiration) => {
            aspirationData.push({
                id: aspiration.id,
                title: aspiration.title,
                argument: aspiration.argument,
                suggestion: aspiration.suggestion,
                proof: aspiration.proof,
                target: aspiration.target,
                isAnonymous: aspiration.isAnonymous,
                createdAt: aspiration.createdAt,
                user: aspiration.isAnonymous? {npm: '', fullname: 'Anonymous'}:aspiration.user,
            });
        });
        res.status(200).send({data: aspirationData});
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.accept = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.update({
        isAccepted: true,
        isActive: true
    },{ where:{id: req.body.aspirationId}}).then(x => {
        console.log(`[aspiration acc][${new Date()}] ${req.npm} accept aspiration #${req.body.aspirationId}`);
        res.status(200).send({ message: "Accepted successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.deny = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.destroy({ where:{id: req.body.aspirationId}}).then(x => {
        console.log(`[aspiration deny][${new Date()}] ${req.npm} deny aspiration #${req.body.aspirationId}`);
        res.status(200).send({ message: "Denied successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.edit = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.update({
        title: req.body.title,
        argument: req.body.argument,
        suggestion: req.body.suggestion,
        proof: req.body.proof,
        target: req.body.target,
    },{ where:{id: req.body.aspirationId}}).then(x => {
        console.log(`[aspiration edit][${new Date()}] ${req.npm} edit aspiration #${req.body.aspirationId}`);
        res.status(200).send({ message: "Updated successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.activate = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.update({
        isActive: true
    },{ where:{id: req.body.aspirationId}}).then(x => {
        console.log(`[aspiration activate][${new Date()}] ${req.npm} activate aspiration #${req.body.aspirationId}`);
        res.status(200).send({ message: "Activated successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.deactivate = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.update({
        isActive: false
    },{ where:{id: req.body.aspirationId}}).then(x => {
        console.log(`[aspiration deactivate][${new Date()}] ${req.npm} deactivate aspiration #${req.body.aspirationId}`);
        res.status(200).send({ message: "Deactivated successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.voterecap = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findOne({
        where: {id: req.query.aspirationId},
        attributes: ['id', 'title', 'isAnonymous'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname']
        },{
            model: Vote,
            attributes: ['userNpm','value'],
            include: {
                model: User,
                include: {model: Class}
            }
        }]
    }).then( aspiration => {
        Class.findAll({ include: {model: User}}).then(classes => {
            let classData = {}
            classes.forEach((clas)=>{
                classData[clas.name] = {
                    classname: clas.name,
                    voted: 0,
                    total: clas.users.length
                }
            })
            aspiration.votes.forEach((vote)=>{
                classData[vote.user.class.name].voted++
            });
            res.status(200).send({data: {
                title: aspiration.title,
                isAnonymous: aspiration.isAnonymous,
                user: aspiration.isAnonymous? {npm: '', fullname: 'Anonymous'}:aspiration.user,
                voterecap: classData}});
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.unvoted = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Aspiration.findAll({
        attributes: ['id', 'title'],
        include: [{
            model: Vote,
            attributes: ['userNpm','value'],
            include: {
                model: User
            }
        }],
        where: {isActive: true}
    }).then( aspirations => {
        User.findAll({include: {model: Class}}).then(users => {
            let unvoters = {}
            aspirations.forEach((aspiration) => {
                users.forEach((user) => {
                    let hasVoted = false
                    aspiration.votes.forEach((vote) => {
                        if(user.npm == vote.userNpm){
                            hasVoted = true
                        }
                    });
                    if(!hasVoted){
                        if(!unvoters[user.npm]){
                            unvoters[user.npm] = {npm: user.npm, fullname: user.fullname, class: user.class.name, unvote: 1}
                        }else{
                            unvoters[user.npm].unvote++
                        }
                    }
                });
            });

            const now = Date.now();
            const csvWriter = createCsvWriter({
                path: `${__dirname}/../export/aspiration/dataunvoters_${now}.csv`,
                header: [
                    { id: "npm", title: "npm" },
                    { id: "fullname", title: "fullname" },
                    { id: "class", title: "class" },
                    { id: "unvote", title: "unvote" }
                ]
            });
            csvWriter.writeRecords(Object.values(unvoters)).then(() => {
                const file = `${__dirname}/../export/aspiration/dataunvoters_${now}.csv`;
                console.log(`[aspiration unvoters download][${new Date()}] ${req.npm} download aspiration unvoters data`);
                res.zip([{
                    path: file,
                    name: `dataunvoters_${now}.csv`
                }]);
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};
