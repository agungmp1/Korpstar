const db = require("../models");
const AchievementRecord = db.achievementrecord;
const Title = db.title;
const Bureau = db.bureau;
const User = db.user;
const Role = db.role;
const Icon = db.icon;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.issuedachievements = (req, res) => {
    User.findOne({
        where: {npm: req.npm},
        attributes: [],
        include: {
            model: Title,
            attributes: ['id', 'bureauId'],
            include: {
                model: Bureau,
                attributes: ['name'],
                include: {
                    model: AchievementRecord,
                    attributes: ['id', 'name', 'type', 'createdAt', 'userNpm'],
                    include: [{
                        model: User,
                        attributes: ['npm', 'fullname', 'class']
                    },{
                        model: Icon,
                        attributes: ['id', 'name', 'filename']
                    }]
                }
            }
        }
    }).then(user => {
        // res.status(500).send({ user: user });
        // console.log(user);
        let achievements = []
        let bureaus = []
        user.titles.forEach((title) => {
            bureaus.push({
                id: title.bureauId,
                name: title.bureau.name
            })
            title.bureau.achievement_records.forEach((achievement) => {
                achievements.push({
                    id: achievement.id,
                    name: achievement.name,
                    type: achievement.type,
                    createdAt: achievement.createdAt,
                    bureau: {
                        id: title.bureauId,
                        name: title.bureau.name
                    },
                    user: {
                        npm: achievement.userNpm,
                        fullname: achievement.user.fullname,
                        class: achievement.user.class
                    },
                    icon: achievement.icon
                });
            });
        });
        res.status(200).send({data: achievements, mybureaus: bureaus});
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.newachievement = (req, res) => {
    // console.log(req.body);
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {npm: req.npm},
        attributes: [],
        include: {
            model: Title,
            attributes: ['id', 'bureauId']
        }
    }).then(user => {
        let isBureauMember = false
        user.titles.forEach((title) => {
            if(req.body.bureauId == title.bureauId) isBureauMember = true
        });
        if (!isBureauMember) {
            return res.status(422).send({ message: "Not member of bureau" })
        }
        AchievementRecord.create({
            name: req.body.name,
            type: req.body.type,
            bureauId: req.body.bureauId,
            iconId: req.body.iconId,
            userNpm: req.body.userNpm
        }).then(x => {
            console.log(`[new achievement][${new Date()}] ${req.npm} issue new achievement #${x.id} to ${req.body.userNpm}`);
            res.status(200).send({ message: "Created sucessfully!" });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.edit = (req, res) => {
    // console.log(req.body);
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    AchievementRecord.findOne({ where: {id: req.body.achievementId}}).then( record => {
        User.findOne({
            where: {npm: req.npm},
            attributes: [],
            include: {
                model: Title,
                attributes: ['id', 'bureauId']
            }
        }).then(user => {
            let isBureauMember = false
            user.titles.forEach((title) => {
                if(record.bureauId == title.bureauId) isBureauMember = true
            });
            if (!isBureauMember) {
                console.log(`invalid privilage from ${req.npm}`);
                return res.status(422).send({ message: "Not member of bureau" })
            }
            AchievementRecord.update({
                name: req.body.name,
                type: req.body.type,
                bureauId: req.body.bureauId,
                iconId: req.body.iconId,
                userNpm: req.body.userNpm
            },{ where: {id: req.body.achievementId}}).then( data => {
                console.log(`[edit achievement][${new Date()}] ${req.npm} edit achievement #${req.body.achievementId} of ${req.body.userNpm}`);
                res.status(200).send({ message: "Updated successfully!" });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.destroy = (req, res) => {
    // console.log(req.body);
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    AchievementRecord.findOne({ where: {id: req.body.achievementId}}).then( record => {
        User.findOne({
            where: {npm: req.npm},
            attributes: [],
            include: {
                model: Title,
                attributes: ['id', 'bureauId']
            }
        }).then(user => {
            let isBureauMember = false
            user.titles.forEach((title) => {
                if(record.bureauId == title.bureauId) isBureauMember = true
            });
            if (!isBureauMember) {
                console.log(`invalid privilage from ${req.npm}`);
                return res.status(422).send({ message: "Not member of bureau" })
            }
            AchievementRecord.destroy({ where: {id: req.body.achievementId}}).then( data => {
                console.log(`[delete achievement][${new Date()}] ${req.npm} delete achievement #${req.body.achievementId}`);
                res.status(200).send({ message: "Destroyed successfully!" });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.myachievements = (req, res) => {
    User.findOne({
        where: {npm: req.npm},
        attributes: [],
        include: [{
            model: Role,
            attributes: ['id']
        },{
            model: AchievementRecord,
            attributes: ['id', 'name', 'bureauId','type', 'createdAt'],
            include: [{
                model: Bureau,
                attributes: ['name']
            },{
                model: Icon,
                attributes: ['filename']
            }]
        }]
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 11) {
                isAdmin = true;
            }
        });
        res.status(200).send({data: user.achievement_records, isAdmin: isAdmin});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.geticons = (req, res) => {
    Icon.findAll().then(icons => {
        res.status(200).send({data: icons});
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}