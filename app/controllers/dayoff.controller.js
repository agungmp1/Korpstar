const db = require("../models");
const User = db.user;
const Class = db.class;
const Peleton = db.peleton;
const Role = db.role;
const Room = db.room;
const Building = db.building;
const DayoffRecord = db.dayoffrecord;
const Config = db.config;

const Op = db.Sequelize.Op;
const csvwriter = require("csv-writer");
const { validationResult } = require('express-validator');
const createCsvWriter = csvwriter.createObjectCsvWriter;
const moment = require('moment')

exports.register = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    if (req.body.pesiartype) {
        if (req.body.pesiartype != 'Pesiar pagi' && req.body.pesiartype != 'Pesiar siang' && req.body.pesiartype != '') {
            return res.status(422).send({ message: "Invalid pesiartype" })
        }
    }
    DayoffRecord.findOne({
        where: {
            [Op.and]: [{
                userNpm: req.npm
            }, {
                date: req.body.date
            }]
        }, include: User
    }).then(record => {
        if (record) {
            return res.status(422).send({ message: "Rule violation!" });
        }
        DayoffRecord.create({
            userNpm: req.npm,
            type: req.body.type,
            date: req.body.date,
            destinationaddress: req.body.destinationaddress,
            selfcontact: req.body.selfcontact,
            destinationcontact: req.body.destinationcontact,
            pesiartype: req.body.pesiartype == '' ? null : req.body.pesiartype,
            transportation: req.body.transportation,
            // status: 'PENDING'
        }).then(user => {
            console.log(`[dayoff register][${new Date()}] ${req.npm} register dayoff`);
            res.status(200).send({ message: "Registered successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
            console.log(err);
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.cancel = (req, res) => {
    // DayoffRecord.findOne({ where: { id: req.body.id } }).then(record => {
    //     if (record.status !== "PENDING" && record.status !== "DECLINED") {
    //         return res.status(422).send({ message: "Rule violation!" });
    //     }

    // }).catch(err => {
    //     res.status(500).send({ message: err.message });
    //     console.log(err);
    // });
    DayoffRecord.destroy({ where: { id: req.body.id } }).then(user => {
        console.log(`[dayoff cancel][${new Date()}] ${req.npm} cancel dayoff`);
        res.status(200).send({ message: "Cancelled successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.list = (req, res) => {
    DayoffRecord.findAll({
        include: {
            model: User,
            attributes: ['fullname'],
            include: [Class, { model: Room, include: Building }, Peleton]
        }
    }).then(dayoffrecords => {
        Config.findOne({ where: { id: 1 } }).then(config => {
            User.findOne({
                where: { npm: req.npm },
                attributes: ['npm'],
                include: {
                    model: Role,
                    attributes: ['id', 'name']
                }
            }).then(user => {
                let isRegistered = false;
                let isAdmin = false;
                user.roles.forEach((role) => {
                    if (role.id == 13) {
                        isAdmin = true;
                    }
                });
                let data = []
                let ownData = {}
                dayoffrecords.forEach((record) => {
                    if (record.userNpm == req.npm) {
                        isRegistered = true;
                        data.unshift({
                            npm: record.userNpm,
                            user: {
                                id: record.id,
                                date: record.date,
                                fullname: record.user.fullname,
                                class: record.user.class.shortname,
                                peleton: record.user.peleton ? record.user.peleton.name : null,
                                room: (!record.user.room) ? null : (record.user.room.building.name + ' ' + record.user.room.number),
                                type: record.type,
                                destinationaddress: record.destinationaddress,
                                selfcontact: record.selfcontact,
                                destinationcontact: record.destinationcontact,
                                pesiartype: record.pesiartype ? record.pesiartype : '-',
                                transportation: record.transportation,
                                // departuretime: record.departuretime,
                                // arrivaltime: record.arrivaltime,
                                // status: record.status,
                                // createdAt: record.createdAt
                            },
                        })
                    } else
                        data.push({
                            npm: record.userNpm,
                            user: {
                                id: record.id,
                                date: record.date,
                                fullname: record.user.fullname,
                                class: record.user.class.shortname,
                                peleton: record.user.peleton ? record.user.peleton.name : null,
                                room: (!record.user.room) ? null : (record.user.room.building.name + ' ' + record.user.room.number),
                                type: record.type,
                                destinationaddress: record.destinationaddress,
                                selfcontact: record.selfcontact,
                                destinationcontact: record.destinationcontact,
                                pesiartype: record.pesiartype ? record.pesiartype : '-',
                                transportation: record.transportation,
                                // departuretime: record.departuretime,
                                // arrivaltime: record.arrivaltime,
                                // status: record.status,
                                // createdAt: record.createdAt
                            },
                        })
                });
                res.status(200).send({
                    data: data,
                    // date: config.dayoffdate,
                    isOpened: config.dayoffopen,
                    isAdmin: isAdmin,
                    isRegistered: isRegistered
                });
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

// exports.accept = (req, res) => {
//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.status(422).send({ message: "Invalid input", errors: errors.array() })
//     }
//     DayoffRecord.findOne({ where: { id: req.body.id } }).then(record => {
//         if (record.status !== "PENDING") {
//             return res.status(422).send({ message: "Rule violation!" });
//         }
//         DayoffRecord.update({
//             status: 'OFF-CAMPUS',
//             // departuretime: moment().format('YYYY-MM-DD hh:mm:ss')
//         }, { where: { id: req.body.id } }).then(user => {
//             console.log(`[dayoff accept][${new Date()}] ${req.npm} accept dayoff of ${record.userNpm}`);
//             res.status(200).send({ message: "Accepted successfully!" });
//         }).catch(err => {
//             console.log(err);
//             res.status(500).send({ message: err.message });
//         });
//     }).catch(err => {
//         res.status(500).send({ message: err.message });
//         console.log(err);
//     });
// };

// exports.deny = (req, res) => {
//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.status(422).send({ message: "Invalid input", errors: errors.array() })
//     }
//     DayoffRecord.findOne({ where: { id: req.body.id } }).then(record => {
//         if (record.status !== "PENDING") {
//             return res.status(422).send({ message: "Rule violation!" });
//         }
//         DayoffRecord.update({
//             status: 'DECLINED'
//         }, { where: { id: req.body.id } }).then(user => {
//             console.log(`[dayoff deny][${new Date()}] ${req.npm} deny dayoff of ${record.userNpm}`);
//             res.status(200).send({ message: "Denied successfully!" });
//         }).catch(err => {
//             console.log(err);
//             res.status(500).send({ message: err.message });
//         });
//     }).catch(err => {
//         res.status(500).send({ message: err.message });
//         console.log(err);
//     });
// };

// exports.setarrived = (req, res) => {
//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.status(422).send({ message: "Invalid input", errors: errors.array() })
//     }
//     DayoffRecord.findOne({ where: { id: req.body.id } }).then(record => {
//         if (record.status !== "OFF-CAMPUS") {
//             return res.status(422).send({ message: "Rule violation!" });
//         }
//         DayoffRecord.update({
//             status: 'ARRIVED',
//             arrivaltime: moment().format('YYYY-MM-DD hh:mm:ss')
//             // arrivaltime: new Date().getTime()
//         }, { where: { id: req.body.id } }).then(user => {
//             console.log(`[dayoff set arrived][${new Date()}] ${req.npm} set arrived dayoff of ${record.userNpm}`);
//             res.status(200).send({ message: "Set successfully!" });
//         }).catch(err => {
//             console.log(err);
//             res.status(500).send({ message: err.message });
//         });
//     }).catch(err => {
//         res.status(500).send({ message: err.message });
//         console.log(err);
//     });
// };

exports.clear = (req, res) => {
    DayoffRecord.destroy({ where: {} }).then(user => {
        console.log(`[dayoff clear][${new Date()}] ${req.npm} reset dayoff`);
        res.status(200).send({ message: "Reset successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const zip = require('express-zip');
exports.download = (req, res) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        DayoffRecord.findAll({
            include: {
                model: User,
                attributes: ['fullname'],
                include: [Class, { model: Room, include: Building }, Peleton]
            }
        }).then(dayoffrecords => {
            array = []
            dayoffrecords.forEach((record) => {
                array.push({
                    npm: record.userNpm,
                    fullname: record.user.fullname,
                    class: record.user.class.name,
                    peleton: record.user.peleton ? record.user.peleton.name : null,
                    room: !record.user.room ? null : (record.user.room.building.name + ' ' + record.user.room.number),
                    type: record.type,
                    date: record.date,
                    destinationaddress: record.destinationaddress,
                    pesiartype: record.pesiartype,
                    transportation: record.transportation,
                    // status: record.status
                })
            });

            const csvWriter = createCsvWriter({
                path: `${__dirname}/../export/dayoffrecord/dataipib_${config.dayoffdate}.csv`,
                header: [
                    { id: "npm", title: "npm" },
                    { id: "fullname", title: "fullname" },
                    { id: "class", title: "class" },
                    { id: "peleton", title: "peleton" },
                    { id: "room", title: "room" },
                    { id: "type", title: "type" },
                    { id: "date", title: "date" },
                    { id: "destinationaddress", title: "destinationaddress" },
                    { id: "pesiartype", title: "pesiartype" },
                    { id: "transportation", title: "transportation" },
                    // { id: "status", title: "status" },
                ]
            });
            csvWriter.writeRecords(array).then(() => {
                const file = `${__dirname}/../export/dayoffrecord/dataipib_${config.dayoffdate}.csv`;
                console.log(`[dayoff download][${new Date()}] ${req.npm} download dayoff data`);
                res.zip([{
                    path: file,
                    name: `dataipib_${config.dayoffdate}.csv`
                }]);
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};
