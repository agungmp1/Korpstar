const db = require("../models");
const User = db.user;
const Class = db.class;
const Role = db.role;
const Locus = db.locus;
const Building = db.building;
const Room = db.room;
const RoomParameter = db.roomparameter;
const RoomViolation = db.roomviolation;
const Config = db.config;
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

function index(val, arr){
    let found = false
    let index = 0
    while (!found && index < arr.length) {
        if(arr[index++].id === val) {
            found = true
            return index-1
        }
    }
    if (!found) return -1
}

exports.getdata = (req, res) => {
    Config.findOne({where: {id: 1} }).then(config => {
        User.findOne({
            where: {npm: req.npm},
            include: {
                model: Role,
                attributes: ['id', 'name']
            }
        }).then( user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 10){
                    isAdmin = true;
                }
            });
            User.findAll({
                where: { roomId: { [Op.ne]: null} },
                attributes: ['npm', 'fullname', 'roomId'],
                include: [{
                    model: Room,
                    attributes: ['number', 'buildingId', 'isChecked'],
                    include: {
                        model: Building,
                        attributes: ['name', 'locuseId'],
                        include: {
                            model: Locus,
                            attributes: ['name']
                        }
                    }
                },{
                    model: Class
                }],
                order: [['roomId','ASC'], ['npm', 'ASC']]
            }).then( users => {
                let locuses = []
                users.forEach((user) => {
                    let locIndex = index(user.room.building.locuseId,locuses)
                    if (locIndex != -1){
                        let buildIndex = index(user.room.buildingId, locuses[locIndex].buildings)
                        if (buildIndex != -1){
                            let roomIndex = index(user.roomId, locuses[locIndex].buildings[buildIndex].rooms)
                            if (roomIndex != -1){
                                locuses[locIndex].buildings[buildIndex].rooms[roomIndex].users.push({
                                    id: user.npm,
                                    class: user.class.name,
                                    fullname: user.fullname
                                })
                            } else {
                                locuses[locIndex].buildings[buildIndex].rooms.push({
                                    id: user.roomId,
                                    number: user.room.number,
                                    users: [{
                                        id: user.npm,
                                        class: user.class.name,
                                        fullname: user.fullname
                                    }],
                                    isChecked: user.room.isChecked
                                })
                            }
                        } else {
                            locuses[locIndex].buildings.push({
                                id: user.room.buildingId,
                                name: user.room.building.name,
                                rooms: [{
                                    id: user.roomId,
                                    number: user.room.number,
                                    users: [{
                                        id: user.npm,
                                        class: user.class.name,
                                        fullname: user.fullname
                                    }],
                                    isChecked: user.room.isChecked
                                }]
                            })
                        }
                    } else {
                        locuses.push({
                            id: user.room.building.locuseId,
                            name: user.room.building.locuse.name,
                            buildings: [{
                                id: user.room.buildingId,
                                name: user.room.building.name,
                                rooms: [{
                                    id: user.roomId,
                                    number: user.room.number,
                                    users: [{
                                        id: user.npm,
                                        class: user.class.name,
                                        fullname: user.fullname
                                    }],
                                    isChecked: user.room.isChecked
                                }]
                            }]
                        })
                    }
                });
                res.status(200).send({
                    data: locuses,
                    isOpened: config.roomcheckingopen,
                    isAdmin: isAdmin,
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        });
    });
};

exports.getlocuses = (req, res) => {
    Locus.findAll().then( locuses => {
        res.status(200).send({data: locuses});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getbuildings = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Building.findAll({
        where: {locuseId: req.query.locuseId}
    }).then( buildings => {
        res.status(200).send(buildings);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getrooms = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Room.findAll({
        where: {buildingId: req.query.buildingId}
    }).then( rooms => {
        res.status(200).send(rooms);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getparameters = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    RoomParameter.findAll().then( params => {
        res.status(200).send({data: params});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.newform = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    roomViolationIds = [];
    try {
        roomViolationIds = JSON.parse(req.body.violations);
    } catch (error) {
        return res.status(422).send({ message: "Invalid input", error: error, input: req.body.violations })
    }
    toSend = [];
    roomViolationIds.forEach((id) => {
        toSend.push({roomId: req.body.roomId, roomParameterId: id});
    });
    Room.findOne({
        where: {id: req.body.roomId},
        attributes: ['isChecked']
    }).then( room => {
        if (room.isChecked) {
            return res.status(403).send({ message: "Room has already been checked"})
        }
        Config.findOne({where: {id: 1} }).then(config => {
            if (req.body.password != config.roomcheckingpassword) {
                return res.status(403).send({ message: "Invalid password"})
            }
            RoomViolation.bulkCreate(toSend).then(() => {
                Room.update({
                    isChecked: true
                },{ where:{id: req.body.roomId}}).then(x => {
                    console.log(`[room checking form][${new Date()}] ${req.npm} checked room ${req.body.roomId}`);
                    res.status(200).send({ message: "Form recorded successfully!" });
                }).catch(err => {
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err.message);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err.message);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err.message);
        res.status(500).send({ message: err.message });
    });
};

exports.getreport = (req, res) => {
    Locus.findAll({
        include: {
            model: Building,
            include: {
                model: Room,
                include: [{
                    model: User,
                    attributes: ['npm', 'fullname']
                },{
                    model: RoomParameter,
                    attributes: ['name', 'point']
                }]
            }
        }
    }).then( locuses => {
        let report = [];
        locuses.forEach((locus) => {
            let locusData = [];
            locus.buildings.forEach((building) => {
                let buildingData = [];
                building.rooms.forEach((room) => {
                    if (room.room_parameters.length > 0) {
                        let point = 0;
                        let violations = [];
                        room.room_parameters.forEach((param) => {
                            point += param.point;
                            violations.push({
                                name: param.name,
                                point: param.point
                            })
                        });
                        buildingData.push({
                            id: room.id,
                            number: room.number,
                            violations: violations,
                            point: point
                        })
                    }
                });
                if (buildingData.length > 0) {
                    locusData.push({
                        name: building.name,
                        rooms: buildingData
                    })
                }
            });
            if (locusData.length > 0) {
                report.push({
                    name: locus.name,
                    buildings: locusData
                })
            }
        });
        res.status(200).send({ data: report });
    }).catch(err => {
        console.log(err.message);
        res.status(500).send({ message: err.message });
    });
};

const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter
const config = require("../config/auth.config.js");

const jwt = require("jsonwebtoken");
const zip = require('express-zip');

exports.download = (req, res) => {
    Config.findOne({where: {id: 1} }).then(config => {
        Room.findAll({
            attributes: ['number', 'buildingId'],
            include: [{
                model: Building,
                attributes: ['name', 'locuseId'],
                include: {
                    model: Locus,
                    attributes: ['name']
                }
            },{
                model: RoomParameter,
                attributes: ['name', 'point']
            }]
        }).then( rooms => {
            let data = []
            rooms.forEach((room) => {
                room.room_parameters.forEach((param) => {
                    data.push({
                        locus: room.building.locuse.name,
                        building: room.building.name,
                        room: room.number,
                        violation: param.name,
                        point: param.point
                    })
                });
            });
            const now = Date.now();
            const csvWriter_room = createCsvWriter({
                path: `${__dirname}/../export/roomcheckingrecord/datapelanggarankamar_${now}.csv`,
                header: [
                    { id: "locus", title: "locus" },
                    { id: "building", title: "building" },
                    { id: "room", title: "room" },
                    { id: "violation", title: "violation" },
                    { id: "point", title: "point" }
                ]
            });
            csvWriter_room.writeRecords(data).then(() => {
                const file = `${__dirname}/../export/roomcheckingrecord/datapelanggarankamar_${now}.csv`;
                console.log(`[room checking download][${new Date()}] ${req.npm} download room checking report`);
                res.zip([{
                    path: file,
                    name: `datapelanggarankamar_${now}.csv`
                }]);
            });
        }).catch(err => {
            console.log(err.message);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err.message);
        res.status(500).send({ message: err.message });
    });
};

exports.formclear = (req, res) => {
    RoomViolation.destroy({ where: {}}).then( data => {
        sequelize.query(`UPDATE rooms SET isChecked = 0;`, { type: Sequelize.QueryTypes.UPDATE }).then( x => {
            console.log(`[room checking reset][${new Date()}] ${req.npm} reset room checking report`);
            res.status(200).send({ message: "Cleared successfully!" });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};
