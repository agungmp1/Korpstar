const db = require("../models");
const User = db.user;
const Item = db.item;
const Role = db.role;
const Config = db.config;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.all = (req, res) => {
    let field = ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'malecommerceopen' : 'femalecommerceopen';
    let gender = ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'Male' : 'Female'
    Item.findAll({
        where: { gender: gender }
    }).then(items => {
        Config.findOne({ where: { id: 1 } }).then(config => {
            User.findOne({
                where: { npm: req.npm },
                include: {
                    model: Role,
                    attributes: ['id']
                }
            }).then(user => {
                let isAdmin = false;
                user.roles.forEach((role) => {
                    if (role.id == 2) {
                        isAdmin = true;
                    }
                });
                let data = [[], []]
                items.forEach(item => {
                    data[(item.stock > 0) ? 0 : 1].push(item)
                });
                res.status(200).send({
                    data: [...data[0], ...data[1]],
                    onStockCount: data[0].length,
                    isOpened: config[field],
                    isAdmin: isAdmin,
                    gender: gender
                });
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.get = (req, res) => {
    const errors = validationRsult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Item.findOne({
        where: { id: req.query.id }
    }).then(item => {
        res.status(200).send(item);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Item.create({
        name: req.body.name,
        stock: req.body.stock,
        price: req.body.price,
        iconId: req.body.iconId,
        gender: ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'Male' : 'Female'
    }).then(user => {
        res.status(200).send({ message: "Item created successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.edit = (req, res) => {
    Item.update({
        name: req.body.name,
        stock: req.body.stock,
        price: req.body.price,
        iconId: req.body.iconId
    }, { where: { id: req.body.id } }).then(item => {
        res.status(200).send({ message: 'Updated successfully' });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.destroy = (req, res) => {
    Item.destroy({ where: { id: req.body.id } }).then(data => {
        res.status(200).send({ message: "Destroyed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.clear = (req, res) => {
    Item.destroy({ where: { gender: ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'Male' : 'Female' } }).then(data => {
        res.status(200).send({ message: "Cleared successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}
