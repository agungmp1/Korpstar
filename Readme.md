# Karya Korps Taruna Poltek SSN Superapp

## Description
API for Karya Korps Taruna, coded in node-express and implementing JWT authentication

## Table of Content
- [Karya Korps Taruna Poltek SSN Superapp](#karya-korps-taruna-poltek-ssn-superapp)
  * [Description](#description)
  * [Table of Content](#table-of-content)
  * [Authorization](#authorization)
    + [Login](#login)
      - [Sent data](#sent-data)
      - [Response](#response)
    + [Authentication](#authentication)
  * [Profile](#profile)
    + [Get Profile](#get-profile)
      - [Response](#response-1)
    + [Get All Locuses](#get-all-locuses)
      - [Response](#response-2)
    + [Get Buildings](#get-buildings)
      - [Sent data](#sent-data-1)
      - [Response](#response-3)
    + [Get Rooms](#get-rooms)
      - [Sent data](#sent-data-2)
      - [Response](#response-4)
    + [Update Room Number](#update-room-number)
      - [Sent data](#sent-data-3)
      - [Response](#response-5)
    + [Change password](#change-password)
      - [Sent data](#sent-data-4)
      - [Response](#response-6)
    + [Download TTE File](#download-tte-file)
      - [Response](#response-7)
    + [Get TTE Password](#get-tte-password)
      - [Response](#response-8)
  * [Fasting](#fasting)
    + [Register](#register)
      - [Response](#response-9)
    + [Cancel](#cancel)
      - [Response](#response-10)
    + [All](#all)
      - [Response](#response-11)
    + [Get Total](#get-total)
      - [Response](#response-12)
    + [Open Fasting](#open-fasting)
      - [Sent data](#sent-data-5)
      - [Response](#response-13)
    + [Close Fasting](#close-fasting)
      - [Response](#response-14)
    + [Clear](#clear)
      - [Response](#response-15)
    + [Download](#download)
      - [Response](#response-16)
    + [Note](#note)
  * [Cyber Warrior Coin](#cyber-warrior-coin)
    + [Get Balance](#get-balance)
      - [Response](#response-17)
    + [Transfer](#transfer)
      - [Sent data](#sent-data-6)
      - [Response](#response-18)
    + [Get My Transfers History](#get-my-transfers-history)
      - [Response](#response-19)
    + [Top Up](#top-up)
      - [Sent data](#sent-data-7)
      - [Response](#response-20)
    + [Get Top Up Requests](#get-top-up-requests)
      - [Response](#response-21)
    + [Top Up Accept](#top-up-accept)
      - [Sent data](#sent-data-8)
      - [Response](#response-22)
    + [Top Up Deny](#top-up-deny)
      - [Sent data](#sent-data-9)
      - [Response](#response-23)
        * [Note](#note-1)
    + [Get My Transaction History](#get-my-transaction-history)
      - [Response](#response-24)
    + [Get All Transaction History](#get-all-transaction-history)
      - [Response](#response-25)
    + [Get My Transaction Detail](#get-my-transaction-detail)
      - [Sent data](#sent-data-10)
      - [Response](#response-26)
        * [on transfers](#on-transfers)
        * [on payment](#on-payment)
        * [on top up](#on-top-up)
  * [E-Commerce](#e-commerce)
    + [Get All Items](#get-all-items)
      - [Response](#response-27)
    + [Get Item](#get-item)
      - [Sent data](#sent-data-11)
      - [Response](#response-28)
    + [Add New Item](#add-new-item)
      - [Sent data](#sent-data-12)
      - [Response](#response-29)
    + [Edit Item Data](#edit-item-data)
      - [Sent data](#sent-data-13)
      - [Response](#response-30)
    + [Delete Item](#delete-item)
      - [Sent data](#sent-data-14)
      - [Response](#response-31)
    + [Clear all Items](#clear-all-items)
      - [Response](#response-32)
    + [Create Order](#create-order)
      - [Sent data](#sent-data-15)
      - [Response](#response-33)
    + [Get All Payment Requests](#get-all-payment-requests)
      - [Response](#response-34)
    + [Payment Request Accept](#payment-request-accept)
      - [Sent data](#sent-data-16)
      - [Response](#response-35)
    + [Payment Request Deny](#payment-request-deny)
      - [Sent data](#sent-data-17)
      - [Response](#response-36)
        * [Note](#note-2)
  * [Foul](#foul)
    + [Get My Foul Records](#get-my-foul-records)
      - [Response](#response-37)
    + [Get All Foul Records](#get-all-foul-records)
      - [Response](#response-38)
    + [Add New Foul Record](#add-new-foul-record)
      - [Sent data](#sent-data-18)
      - [Response](#response-39)
  * [Research](#research)
    + [Get All Researches](#get-all-researches)
      - [Response](#response-40)
    + [Add New Research](#add-new-research)
      - [Sent data](#sent-data-19)
      - [Response](#response-41)
    + [Edit Research Data](#edit-research-data)
      - [Sent data](#sent-data-20)
      - [Response](#response-42)
    + [Delete Research](#delete-research)
      - [Sent data](#sent-data-21)
      - [Response](#response-43)
    + [Clear all Research](#clear-all-research)
      - [Response](#response-44)
  * [Jarinan Dasar Ilmu Hukum (?)](#jarinan-dasar-ilmu-hukum----)
    + [Get All JDIH](#get-all-jdih)
      - [Response](#response-45)
    + [Download JDIH](#download-jdih)
      - [Params](#params)
      - [Response](#response-46)
    + [Add New File](#add-new-file)
      - [Sent data](#sent-data-22)
      - [Response](#response-47)
      - [Sent data](#sent-data-23)
      - [Response](#response-48)
    + [Delete File](#delete-file)
      - [Sent data](#sent-data-24)
      - [Response](#response-49)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Authorization

### Login
[POST] /api/auth/signin
#### Sent data
```Json
{
    "npm": "INSERT_NPM",
    "password": "INSERT_PASSWORD_SHA1_DIGEST"
}
```
#### Response
```json
{
    "npm": "USER_NPM",
    "accessToken": "BEARER_TOKEN"
}
```

### Authentication
To access any other path than login, use Headers
```json
{
    "Authorization": "Bearer INSERT_BEARER_TOKEN",
}
```

## Profile
### Get Profile
[GET] /api/user/profile
#### Response
```json
{
    "data": {
        "npm": "2019101609",
        "fullname": "Herlambang Rafli Wicaksono",
        "class": "2 RPLK",
        "room": {
            "string": "Asrama Putra A 418",
            "number": "418",
            "building": "Asrama Putra A",
            "locus": "Ciseeng"
        }
    }
}
```
### Get All Locuses
[GET] /api/locuse/all
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "name": "Bojongsari"
        },
        {
            "id": 1,
            "name": "Ciseeng"
        }
    ]
}
```
### Get Buildings
[GET] /api/building/list
#### Sent data
|  params  | example value |
|:--------:|:-------------:|
| locuseId |       1       |
#### Response
```json
[
    {
        "id": 1,
        "name": "Asrama Putra A",
        "locuseId": 1
    },
    {
        "id": 2,
        "name": "Asrama Putra B",
        "locuseId": 1
    }
]
```
### Get Rooms
[GET] /api/room/list
#### Sent data
|   params   | example value |
|:----------:|:-------------:|
| buildingId |       1       |
#### Response
```json
[
    {
        "id": 1,
        "number": "101",
        "buildingId": 1
    },
    {
        "id": 2,
        "number": "102",
        "buildingId": 1
    }
]
```
### Update Room Number
[POST] /api/user/update
#### Sent data
```json
{
    "room": "ROOM_ID"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Change password
[POST] /api/auth/changePassword
#### Sent data
```json
{
    "password": "INSERT_PASSWORD_SHA1_DIGEST",
    "newPassword": "INSERT_NEW_PASSWORD_SHA1_DIGEST"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Download TTE File
[GET] /api/user/digitalsignaturefile  
or  
[GET] /api/user/digitalsignaturefile/ACCESS_TOKEN without auth header
#### Response
A file named ```NAME.p12``` or
```json
{
    "message": "MESSAGE"
}
```
### Get TTE Password
[GET] /api/user/digitalsignaturepassword
#### Response
```json
{
    "password": "DIGITAL_SIGNATURE_PASSWORD"
}
```

## Fasting
### Register
[POST] /api/fasting/register
#### Response
```json
{
    "message": "MESSAGE"
}
```

### Cancel
[POST] /api/fasting/cancel
#### Response
```json
{
    "message": "MESSAGE"
}
```

### All
[GET] /api/fasting/list
#### Response
```json
{
    "data": [
        {
            "npm": "1817101467",
            "user": {
                "fullname": "Zathalinny",
                "class": "4 RPK Quantum"
            }
        },
        {
            "npm": "1817101468",
            "user": {
                "fullname": "Zidna Wildan Alfain",
                "class": "4 RPLK"
            }
        },
        {
            "npm": "2019101600",
            "user": {
                "fullname": "Faqih Rangga Wijaya",
                "class": "2 RPK "
            }
        }
    ],
    "date": "Sat, 09 Oct 2021",
    "isOpened": true,
    "isAdmin": false,
    "isRegistered": false
}
```

### Get Total
[GET] /api/fasting/total
#### Response
```json
{
    "total": 100,
    "isRegistered": true
}
```

### Open Fasting
[fasting admin]  
[POST] /api/config/openfasting
#### Sent data
```json
{
    "fastingdate": "DATE_TO_OPEN_FASTING"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```

### Close Fasting
[fasting admin]  
[POST] /api/config/closefasting
#### Response
```json
{
    "message": "MESSAGE"
}
```

### Clear
[fasting admin]  
[POST] /api/fasting/clear
#### Response
```json
{
    "message": "MESSAGE"
}
```

### Download
[fasting admin]  
[GET] /api/fasting/download
#### Response
A file named ```datapuasa_DATE.csv``` or
```json
{
    "message": "MESSAGE"
}
```

### Note
If the fasting fiture is turned off in the configuration, a 403 response with json will be sent
```json
{
    "message": "Routes is closed!"
}
```

## Cyber Warrior Coin
### Get Balance
[GET] /api/coin/balance
#### Response
```json
{
    "balance": 50000
}
```
### Transfer
[POST] /api/coin/transfer
#### Sent data
```json
{
    "targetNpm": 2019101609,
    "value": 50000
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Get My Transfers History
[GET] /api/coin/mytransfers
#### Response
```json
{
    "data": {
        "sent": [
            {
                "reciever": {
                    "npm": "2019101648",
                    "fullname": "Rayhan Ramdhany Hanaputra"
                },
                "value": 5000,
                "date": "2021-10-22 07:09:26"
            }
        ],
        "recieved": [
            {
                "sender": {
                    "npm": "2019101648",
                    "fullname": "Rayhan Ramdhany Hanaputra"
                },
                "value": 10000,
                "date": "2021-10-22 06:53:06"
            }
        ]
    },
    "isAdmin": true
}
```
### Top Up
[POST] /api/coin/topup
#### Sent data
```json
{
    "value": 50000
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Get Top Up Requests
[GET] /api/coin/alltopup
#### Response
```json
{
    "data": [
        {
            "id": 37,
            "npm": "2019101648",
            "createdAt": "2021-10-22 20:45:44",
            "value": 300000,
            "user": {
                "fullname": "Rayhan Ramdhany Hanaputra",
                "class": "II Rekayasa Perangkat Lunak Kripto"
            }
        },{
            "id": 37,
            "npm": "2019101648",
            "createdAt": "2021-10-22 20:45:44",
            "value": 300000,
            "user": {
                "fullname": "Rayhan Ramdhany Hanaputra",
                "class": "II Rekayasa Perangkat Lunak Kripto"
            }
        }
    ]
}
```
### Top Up Accept
[coin admin]  
[POST] /api/coin/accept
#### Sent data
```json
{
    "id": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Top Up Deny
[coin admin]  
[POST] /api/coin/deny
#### Sent data
```json
{
    "id": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
##### Note
Top up requests can be accepted or denied if and only if the current status is "PENDING"

### Get My Transaction History
[GET] /api/coin/history
#### Response
```json
{
    "data": [
        {
            "id": 5,
            "npm": "2019101609",
            "value": 50000,
            "type": "TOP UP",
            "status": "SUCCESS",
            "createdAt": "2021-10-12T07:14:56.000Z",
            "updatedAt": "2021-10-12T07:15:21.000Z"
        },
        {
            "id": 6,
            "npm": "2019101609",
            "value": 10000,
            "type": "PAYMENT",
            "status": "SUCCESS",
            "createdAt": "2021-10-12T07:15:57.000Z",
            "updatedAt": "2021-10-12T07:15:57.000Z"
        }
    ],
    "isAdmin": true,
    "balance": 50000
}
```
### Get All Transaction History
[coin admin]  
[GET] /api/coin/allhistory
#### Response
```json
{
    "data": [
        {
            "id": 5,
            "npm": "2019101609",
            "value": 50000,
            "type": "TOP UP",
            "status": "SUCCESS",
            "createdAt": "2021-10-12T07:14:56.000Z",
            "updatedAt": "2021-10-12T07:15:21.000Z"
        },
        {
            "id": 6,
            "npm": "2019101609",
            "value": 10000,
            "type": "PAYMENT",
            "status": "SUCCESS",
            "createdAt": "2021-10-12T07:15:57.000Z",
            "updatedAt": "2021-10-12T07:15:57.000Z"
        }
    ]
}
```
### Get My Transaction Detail
[GET] /api/coin/transactiondetail
#### Sent data
| params | example value |
|:------:|:-------------:|
|   id   |       1       |
#### Response
##### on transfers
```json
{
    "data": {
        "id": 133,
        "type": "TRANSFER",
        "value": 25000,
        "status": "SUCCESS",
        "createdAt": "2021-10-23 08:42:10",
        "updatedAt": "2021-10-23 08:42:10",
        "user": {
            "npm": "2019101648",
            "fullname": "Rayhan Ramdhany Hanaputra"
        },
        "reciever": {
            "npm": "2019101644",
            "fullname": "Nurfarida Sekar Andzani"
        },
        "orders": []
    }
}
```
##### on payment
```json
{
    "data": {
        "id": 132,
        "type": "PAYMENT",
        "value": 83500,
        "status": "PENDING",
        "createdAt": "2021-10-23 08:41:43",
        "updatedAt": "2021-10-23 08:41:43",
        "user": {
            "npm": "2019101648",
            "fullname": "Rayhan Ramdhany Hanaputra"
        },
        "reciever": null,
        "orders": [
            {
                "quantity": 1,
                "subtotal": 83500,
                "item": {
                    "id": 4,
                    "name": "Dancow Susu Sachet 800gr"
                }
            }
        ]
    }
}
```
##### on top up
```json
{
    "data": {
        "id": 131,
        "type": "TOP UP",
        "value": 300000,
        "status": "SUCCESS",
        "createdAt": "2021-10-23 08:41:20",
        "updatedAt": "2021-10-23 08:41:26",
        "user": {
            "npm": "2019101648",
            "fullname": "Rayhan Ramdhany Hanaputra"
        },
        "reciever": null,
        "orders": []
    }
}
```
## E-Commerce
### Get All Items
[GET] /api/item/all
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "name": "Indomilk Susu Sachet 800gr",
            "stock": 5,
            "price": 68500,
            "createdAt": "2021-10-13T02:44:05.000Z",
            "updatedAt": "2021-10-13T02:44:05.000Z"
        },
        {
            "id": 3,
            "name": "Indomilk Susu Sachet 800gr",
            "stock": 5,
            "price": 68500,
            "createdAt": "2021-10-14T01:48:25.000Z",
            "updatedAt": "2021-10-14T01:48:25.000Z"
        }
    ]
}
```
### Get Item
[GET] /api/item/get
#### Sent data
| params | example value |
|:------:|:-------------:|
|   id   |       1       |
#### Response
```json
{
    "id": 3,
    "name": "Indomilk Susu Sachet 800gr",
    "stock": 5,
    "price": 68500,
    "createdAt": "2021-10-14T01:48:25.000Z",
    "updatedAt": "2021-10-14T01:48:25.000Z"
}
```
### Add New Item
[commerce admin]  
[POST] /api/item/create
#### Sent data
```json
{
    "name": "Dancow Susu Sachet 800gr",
    "price": 89500,
    "stock": 10
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Edit Item Data
[commerce admin]  
[POST] /api/item/edit
#### Sent data
```json
{
    "id": 4,
    "name": "Dancow Susu Sachet 800gr",
    "price": 83500,
    "stock": 10
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Delete Item
[commerce admin]  
[POST] /api/item/destroy
#### Sent data
```json
{
    "id": 4
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Clear all Items
[commerce admin]  
[POST] /api/item/clear
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Create Order
[POST] /api/order/create
#### Sent data
```json
{
    "orders": [{
        "itemId": 2,
        "quantity": 1
    },{
        "itemId": 4,
        "quantity": 1
    }]
}
```
#### Response
```json
{
    "npm": 2091203910,
    "fullname": "mamat",
    "orders": [
        {
            "itemId": 4,
            "itemName": "kodok",
            "quantity": 34,
            "subtotal": 89500
        }
    ],
    "transaction": {
        "time": "2021-10-14T02:05:36.467Z",
        "total": 89500,
        "id": 34
    },
    "message": "Order request success!"
}
```
or if failed
```json
{
    "message": "MESSAGE"
}
```
### Get All Payment Requests
[commerce admin]  
[GET] /api/payment/all
#### Response
```json
{
    "data": [
        {
            "id": 132,
            "npm": "2019101648",
            "value": 83500,
            "type": "PAYMENT",
            "status": "PENDING",
            "createdAt": "2021-10-23 08:41:43",
            "updatedAt": "2021-10-23 08:41:43",
            "user": {
                "npm": "2019101648",
                "fullname": "Rayhan Ramdhany Hanaputra"
            },
            "orders": [
                {
                    "quantity": 1,
                    "subtotal": 83500,
                    "item": {
                        "name": "Dancow Susu Sachet 800gr"
                    }
                }
            ]
        }
    ]
}
```
### Payment Request Accept
[commerce admin]  
[POST] /api/payment/accept
#### Sent data
```json
{
    "id": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Payment Request Deny
[commerce admin]  
[POST] /api/payment/deny
#### Sent data
```json
{
    "id": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
##### Note
Payment requests can be accepted or denied if and only if the current status is "PENDING"

## Foul
### Get My Foul Records
[GET] /api/foul/list
#### Response
```json
{
    "fouls": [
        {
            "id": 2,
            "date": "2021-12-21",
            "npm": "2019101648",
            "foulId": 1,
            "foul": {
                "id": 1,
                "name": "Hukuman Taruna Jaga"
            }
        }
    ]
}
```
### Get All Foul Records
[foul admin]  
[GET] /api/foul/all
#### Response
```json
{
    "userfouls": [
        {
            "id": 1,
            "date": "2021-12-21",
            "npm": "2019101609",
            "foulId": 2,
            "user": {
                "npm": "2019101609",
                "fullname": "Herlambang Rafli Wicaksono",
                "class": "2 RPLK"
            },
            "foul": {
                "id": 2,
                "name": "Terlambat Kegiatan"
            }
        },
        {
            "id": 2,
            "date": "2021-12-21",
            "npm": "2019101648",
            "foulId": 1,
            "user": {
                "npm": "2019101648",
                "fullname": "Rayhan Ramdhany Hanaputra",
                "class": "2 RPLK"
            },
            "foul": {
                "id": 1,
                "name": "Hukuman Taruna Jaga"
            }
        }
    ]
}
```
### Add New Foul Record
[foul admin]  
[POST] /api/foul/add
#### Sent data
```json
{
    "npm": 2019101648,
    "foulId": 1,
    "date": "Mon, 21 December 2021"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```

## Research
### Get All Researches
[GET] /api/research/all
#### Response
```json
{
    "data": [
        {
            "id": 1,
            "researcher": "Muhammad Rakha Rafi Bayhaqi",
            "conference": "The 16th International Conference on Quality in Research 2019 (QIR)",
            "paper": "Correcting Block Attack on Reduced NEEVA",
            "date": "22-24 July 2019",
            "location": "Sumatera Barat"
        },
        {
            "id": 2,
            "researcher": "Mohammad Heading Nor Ilah",
            "conference": "The 16th International Conference on Quality in Research 2019 (QIR)",
            "paper": "Collision Attack On 4 Secure PGV Hash Function Schemes based on 4-Round PRESENT-80 with Iterative Differential Approach",
            "date": "22-24 July 2019",
            "location": "Sumatera Barat"
        }
    ],
    "isAdmin": true
}
```
### Add New Research
[research admin]  
[POST] /api/research/create
#### Sent data
```json
{
    "researcher": "Muhammad Rakha Rafi Bayhaqi",
    "conference": "The 16th International Conference on Quality in Research 2019 (QIR)",
    "date": "22-24 July 2019",
    "location": "Sumatera Barat",
    "paper": "Correcting Block Attack on Reduced NEEVA"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Edit Research Data
[research admin]  
[POST] /api/research/edit
#### Sent data
```json
{
    "id": 2,
    "researcher": "Mohammad Heading Nor Ilah",
    "conference": "The 16th International Conference on Quality in Research 2019 (QIR)",
    "date": "22-24 July 2019",
    "location": "Sumatera Barat",
    "paper": "Collision Attack On 4 Secure PGV Hash Function Schemes based on 4-Round PRESENT-80 with Iterative Differential Approach"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Delete Research
[research admin]  
[POST] /api/research/destroy
#### Sent data
```json
{
    "id": 2
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Clear all Research
[research admin]  
[POST] /api/research/clear
#### Response
```json
{
    "message": "MESSAGE"
}
```

## Jarinan Dasar Ilmu Hukum (?)
### Get All JDIH
[GET] /api/jdih/all
#### Response
```json
{
    "data": [
        {
            "id": 1,
            "name": "test",
            "file": "korpstar.png",
            "createdAt": "2021-10-17T06:48:19.000Z",
            "updatedAt": "2021-10-17T06:48:19.000Z"
        }
    ]
}
```
### Download JDIH
[GET] /api/jdih/download
#### Params
| params | example value |
|:------:|:-------------:|
|   id   |       1       |
#### Response
A file named ```FILENAME.EXTENSION``` or
```json
{
    "message": "MESSAGE"
}
```

### Add New File
[JDIH admin]  
[POST] /api/jdih/addfile
#### Sent data
A file named ```FILENAME.EXTENSION```
#### Response
```json
{
    "filename": "UPLOADED_FILE_NAME"
}
```
use the recieved json to add description
[POST] /api/jdih/adddesc
#### Sent data
```json
{
    "name": "test",
    "filename": "RECIEVED_FILE_NAME"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Delete File
[JDIH admin]  
[POST] /api/jdih/delete
#### Sent data
```json
{
    "id": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
