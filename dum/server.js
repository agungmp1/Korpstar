const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var https = require('https');
var fs = require('fs');
const corsOptions = {
	origin: 'http://localhost',
	'Access-Control-Allow-Origin': 'http://localhost',
	withCredentials: false,
	'access-control-allow-credentials': true,
	credentials: false,
	optionSuccessStatus: 200,
}
const app = express();
const busboy = require('connect-busboy');
app.use(busboy());
// var corsOptions = {
//     origin: "http://localhost:8080"
// };

app.use(cors()); //corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
	extended: true
}));

// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', 'https://karya.korpstar-poltekssn.org');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });

const db = require("./app/models");

// // db.sequelize.sync({ force: true }).then(() => {
// //     console.log('Drop and Resync Db');
// try {
// db.sequelize.sync().then(() => {
// 	// initial();
// }).catch(err => {
// 	console.log(err.message);
// });
// } catch (e) {
//     console.log(e);
// }

const cron = require('node-cron');
const fsExtra = require('fs-extra')

const bot = require("./app/utils/telebot.js");
bot.launch();
// bot.sendMessage();
// require('events').EventEmitter.defaultMaxListeners = 0

const {
	autoheal,
	cancelfoul
} = require("./app/controllers/foul.controller.js");
// cancelfoul()

const {
	clearInactive
} = require("./app/controllers/fcm.controller.js");

const {
	notifynotsubmitters
} = require("./app/controllers/class.controller.js");

const {
	resetPasswordServer
} = require("./app/controllers/auth.controller.js");

// resetPasswordServer('1918101511')
// fcm.sendNotification('dhUUeHbtWMUhuYkVRI8kP_:APA91bGC0JIoW74TXnS28q-IWx8RpzmZWRu8Py8ffvpZSr4gfGdjfoj8IdC_of0Ddl6dEIXJSGH8RJnht_rgCfGd-ct7TXhokvI9MeY6nO-XMG0DpfhwLPg4EGhKQ0fdoSoKNjXJReT3', {notification: {title: 'e', body: 'a'}})

cron.schedule('0 0 * * *', function () {
	console.log('---------------------');
	console.log('Running Daily Cron Job');
	fsExtra.emptyDirSync('./app/export/fastingrecord', err => {
		console.log(err);
	});
	fsExtra.emptyDirSync('./app/export/roomcheckingrecord', err => {
		console.log(err);
	});
	fsExtra.emptyDirSync('./app/export/aspiration', err => {
		console.log(err);
	});
	fsExtra.emptyDirSync('./app/export/feedbackrecord', err => {
		console.log(err);
	});
	bot.launch()
	autoheal()
	clearInactive()
});

cron.schedule('0 17 * * 1-5', function () {
	console.log('---------------------');
	console.log(`[${new Date()}] Running Cron Job for wajar notif`);
	notifynotsubmitters() //wajar
});

// simple route
app.get("/", (req, res) => {
	res.json({
		message: "Welcome to bezkoder application."
	});
});
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/room.routes')(app);
require('./app/routes/achievement.routes')(app);
require('./app/routes/achievement.routes')(app);
require('./app/routes/fastingrecord.routes')(app);
require('./app/routes/dayoff.routes')(app);
require('./app/routes/class.routes')(app);
require('./app/routes/item.routes')(app);
require('./app/routes/order.routes')(app);
require('./app/routes/config.routes')(app);
require('./app/routes/coin.routes')(app);
require('./app/routes/payment.routes')(app);
require('./app/routes/research.routes')(app);
require('./app/routes/foul.routes')(app);
require('./app/routes/jdih.routes')(app);
require('./app/routes/ctf.routes')(app);
require('./app/routes/aspiration.routes')(app);
require('./app/routes/static.routes')(app);
require('./app/routes/fcm.routes')(app);
require('./app/routes/feedback.routes')(app);
require('./app/routes/book.routes')(app);
require('./app/routes/library.routes')(app);

var https_options = {
	key: fs.readFileSync("/etc/ssl/wildcard/private.key"),
	cert: fs.readFileSync("/etc/ssl/wildcard/star_korpstar-poltekssn_org.crt"),
	ca: [
		fs.readFileSync('/etc/ssl/wildcard/star_korpstar-poltekssn_org.crt'),
		fs.readFileSync('/etc/ssl/wildcard/Chain_RootCA_Bundle.crt')
	]
};

// const socket = require('./app/socket/');
//socket.runsocket(app);

// Create an HTTPS service identical to the HTTP service.
const server = https.createServer(https_options, app).listen(8443);

// //set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});


// socket.runsocket(server);

// const User = db.user;
// const Class = db.class;
// const FastingRecord = db.fastingrecord;
// const Role = db.role;

// const {
// 	users
// } = require('./peleton.js');

function initial() {
	// users.forEach(user => {
	// 	User.update({peletonId: user.peletonId},{
	// 		where: {npm: user.npm}
	// 	})
	// });
	// Class.bulkCreate(data).then(x => {
	//     console.log("class created");
	// });

	// Class.findAll().then( classes => {
	//     User.findAll({attributes: ['npm', 'classname']}).then( users => {
	//         users.forEach((user) => {
	//             let classId = 1;
	//             classes.forEach((classe) => {
	//                 if (user.classname == classe.name){
	//                     classId = classe.id
	//                 }
	//             });
	//             User.update({classId: classId}, {where: {npm: user.npm}})
	//         });
	//         console.log("success");
	//     })
	// })
}

process.on('SIGINT', () => { console.log("Bye bye!"); process.exit(); });